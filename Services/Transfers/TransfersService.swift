//
//  TransfersService.swift
//  Base
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import Foundation

public enum FundType: String, Decodable {
    case balance = "BALANCE"
}

public enum TransfersEndpoint: Pathable {
    case create

    public var path: String {
        switch self {
        case .create:
            return "transfers"
        }
    }
}

public enum FundEndpoint: V3Pathable {
    case fund(profileId: Int, transferId: Int)

    public var path: String {
        switch self {
        case let .fund(profileId, transferId):
            return "profiles/\(profileId)/transfers/\(transferId)/payments"
        }
    }
}

public class TransfersService {
    public typealias CreateHandler = (Transfer?, Error?) -> Void
    public typealias FundHandler = (FundResponse?, Error?) -> Void
    private let client: RestClient

    init(client: RestClient) {
        self.client = client
    }

    public func create(targetAccount: Int,
                       quote: Int,
                       customerTransactionId: String,
                       reference: String?,
                       transferPurpose: String,
                       sourceOfFunds: String,
                       completion: @escaping CreateHandler) {
        let data: [String: Any] = [
            "targetAccount": targetAccount,
            "quote": quote,
            "customerTransactionId": customerTransactionId,
            "details": [
                "reference": reference,
                "transferPurpose": transferPurpose,
                "sourceOfFunds": sourceOfFunds
            ]
        ]

        client.post(endpoint: TransfersEndpoint.create, data: data) { data, error in
            if let error = error {
                completion(nil, error)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(DateFormatter.medium)
                    let transfer = try decoder.decode(Transfer.self, from: data)
                    completion(transfer, nil)
                } catch {
                    completion(nil, RestClientError.cannotDecode(error: error))
                }
            } else {
                completion(nil, RestClientError.noData)
            }
        }
    }

    public func fund(transfer: Int, profile: Int, type: FundType = .balance, completion: @escaping FundHandler) {
        let data = ["type": type.rawValue]

        client.post(endpoint: FundEndpoint.fund(profileId: profile, transferId: transfer), data: data) { data, error in
            if let error = error {
                completion(nil, error)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let transfer = try decoder.decode(FundResponse.self, from: data)
                    completion(transfer, nil)
                } catch {
                    completion(nil, RestClientError.cannotDecode(error: error))
                }
            } else {
                completion(nil, RestClientError.noData)
            }
        }
    }
}
