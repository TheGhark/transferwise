//
//  Transfer.swift
//  Base
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

public struct TransferDetails: Decodable {
    let reference: String
}

public struct Transfer: Decodable {
    let id: Int
    let user: Int
    let targetAccount: Int
    let sourceAcount: Int?
    let quote: Int
    let status: String
    let reference: String?
    let rate: Double
    let created: Date
    let business: Int?
    let transferRequest: Int?
    let details: TransferDetails
    let hasActiveIssues: Bool
    let sourceCurrency: String
    let sourceValue: Double
    let targetCurrency: String
    let targetValue: Double
    let customerTransactionId: UUID
}
