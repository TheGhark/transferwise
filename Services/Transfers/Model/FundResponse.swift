//
//  FundResponse.swift
//  Base
//
//  Created by Camilo Gaviria on 24/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

public enum FundStatus: String, Decodable {
    case completed = "COMPLETED"
    case rejected = "REJECTED"
}

public struct FundResponse: Decodable {
    let type: FundType
    let status: FundStatus
    let errorCode: String?
}
