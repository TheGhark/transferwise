//
//  OpenExchangeRatesService.swift
//  TransferWiseAPI
//
//  Created by Camilo Gaviria on 16/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import Foundation

enum OpenExchangeRatesEndpoint: Pathable {
    case currencies

    var path: String {
        switch self {
        case .currencies:
            return "currencies.json"
        }
    }
}

open class OpenExchangeRatesService {
    public typealias CompletionHandler = ([String: String]?, Error?) -> Void
    private let client: RestClient

    static var standard: OpenExchangeRatesService {
        let bundle = Bundle(for: OpenExchangeRatesService.self)
        let configuration = Configuration(bundle: bundle, name: "OERConfiguration")
        let client = RestClient(configuration: configuration)
        return OpenExchangeRatesService(client: client)
    }

    public init(client: RestClient) {
        self.client = client
    }

    open func currencies(completion: @escaping CompletionHandler) {
        client.get(endpoint: OpenExchangeRatesEndpoint.currencies) { data, error in
            if let error = error {
                completion(nil, error)
            } else if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
                    completion(json, nil)
                } catch {
                    completion(nil, RestClientError.cannotDecode(error: error))
                }
            } else {
                completion(nil, RestClientError.noData)
            }
        }
    }
}
