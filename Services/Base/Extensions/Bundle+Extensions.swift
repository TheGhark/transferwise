//
//  Bundle+Extensions.swift
//  TransferWiseAPI
//
//  Created by Camilo Gaviria on 14/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

extension Bundle {
    func loadConfiguration(name: String) -> [String: AnyObject]? {
        var propertyListFormat = PropertyListSerialization.PropertyListFormat.xml
        guard
            let path = self.path(forResource: name, ofType: "plist"),
            let data = FileManager.default.contents(atPath: path)
        else {
            fatalError("Cannot find configuration file")
        }

        do {
            return try PropertyListSerialization.propertyList(from: data,
                                                              options: .mutableContainersAndLeaves,
                                                              format: &propertyListFormat) as? [String: AnyObject]

        } catch {
            fatalError("Cannot load configuration.plist")
        }
    }
}
