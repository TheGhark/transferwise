//
//  ProfileType.swift
//  Base
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

public enum ProfileType: String, Decodable {
    case personal = "PERSONAL"
    case business = "BUSINESS"
}
