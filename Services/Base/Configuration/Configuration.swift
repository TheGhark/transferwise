//
//  Configuration.swift
//  TransferWiseAPI
//
//  Created by Camilo Gaviria on 14/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

public struct Configuration {
    let token: String
    let baseURL: String
    let baseV3URL: String

    private var configuration: [String: AnyObject]?

    public init(token: String? = nil, bundle: Bundle = Bundle.main, name: String) {
        configuration = bundle.loadConfiguration(name: name)

        if let baseURL = configuration?["BaseURL"] as? String {
            self.baseURL = baseURL
        } else {
            baseURL = ""
        }

        if let baseV3URL = configuration?["BaseV3URL"] as? String {
            self.baseV3URL = baseV3URL
        } else {
            baseV3URL = ""
        }

        if let token = token {
            self.token = token
        } else if let token = configuration?["APIKey"] as? String {
            self.token = token
        } else {
            self.token = ""
        }
    }
}
