//
//  RestClient.swift
//  TransferWiseAPI
//
//  Created by Camilo Gaviria on 14/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

public protocol Pathable {
    var path: String { get }
}

public protocol V3Pathable: Pathable {}

public enum RestClientError: Error, Equatable {
    case noData
    case cannotDecode(error: Error?)
    case invalidToken
    case noRequest
    case service(error: Error?)

    public static func == (lhs: RestClientError, rhs: RestClientError) -> Bool {
        switch (lhs, rhs) {
        case (.noData, .noData):
            return true
        case (.cannotDecode(error: _), .cannotDecode(error: _)):
            return true
        case (.invalidToken, .invalidToken):
            return true
        case (.noRequest, .noRequest):
            return true
        case (.service(error: _), .service(error: _)):
            return true
        default:
            return false
        }
    }
}

enum HTTPMethod: String {
    case get
    case post
}

public class RestClient {
    // MARK: - Class Functions

    // MARK: - Properties

    public typealias CompletionHandler = (Data?, RestClientError?) -> Void

    private let configuration: Configuration
    private let session: URLSession

    // MARK: - Computed Properties

    private var token: String {
        return configuration.token
    }

    private var baseURL: String {
        return configuration.baseURL
    }

    private var baseV3URL: String {
        return configuration.baseV3URL
    }

    // MARK: - Initialization

    public init(configuration: Configuration, session: URLSession? = nil) {
        self.configuration = configuration
        self.session = session ?? URLSession(configuration: .default)
    }

    // MARK: - Public

    public func get(endpoint: Pathable, queryItems: [URLQueryItem]? = nil, completion: @escaping CompletionHandler) {
        guard let request = self.request(method: .get, endpoint: endpoint, queryItems: queryItems) else {
            completion(nil, .noRequest)
            return
        }

        session.dataTask(with: request) { data, _, error in
            if let error = error {
                completion(nil, .service(error: error))
            } else if let data = data {
                completion(data, nil)
            } else {
                completion(nil, .noData)
            }
        }.resume()
    }

    public func post(endpoint: Pathable, queryItems: [URLQueryItem]? = nil, data: [String: Any]? = nil, completion: @escaping CompletionHandler) {
        guard var request = self.request(method: .post, endpoint: endpoint, queryItems: queryItems) else {
            completion(nil, .noRequest)
            return
        }

        if let data = data {
            do {
                try request.httpBody = JSONSerialization.data(withJSONObject: data,
                                                              options: .prettyPrinted)
            } catch {
                completion(nil, .noData)
                return
            }
        }

        session.dataTask(with: request) { data, _, error in
            if let error = error {
                completion(nil, .service(error: error))
            } else if let data = data {
                completion(data, nil)
            } else {
                completion(nil, .noData)
            }
        }.resume()
    }

    // MARK: - Private

    private func request(method: HTTPMethod, endpoint: Pathable, queryItems: [URLQueryItem]? = nil) -> URLRequest? {
        let path: String
        let baseURL: String

        if let v3Endpoint = endpoint as? V3Pathable {
            path = v3Endpoint.path
            baseURL = baseV3URL
        } else {
            path = endpoint.path
            baseURL = self.baseURL
        }

        guard
            let url = URL(string: "\(baseURL)/\(path)"),
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        else {
            return nil
        }

        components.queryItems = queryItems

        guard let fullURL = components.url else {
            return nil
        }

        var request = URLRequest(url: fullURL)
        request.httpMethod = method.rawValue.uppercased()
        request.setValue("Bearer \(configuration.token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }

    // MARK: - Overridden
}
