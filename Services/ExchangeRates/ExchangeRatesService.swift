//
//  ExchangeRatesService.swift
//  QuotesTests
//
//  Created by Camilo Gaviria on 24/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import Foundation

enum ExchangeRatesEndpoint: Pathable {
    case rates

    var path: String {
        switch self {
        case .rates:
            return "rates"
        }
    }
}

public class ExchangeRatesService {
    // MARK: - Class Functions

    // MARK: - Properties

    public typealias CompletionHandler = (RatesResponse?, Error?) -> Void

    private let client: RestClient

    // MARK: - Computed Properties

    // MARK: - Initialization

    public init(client: RestClient) {
        self.client = client
    }

    // MARK: - Public

    public func rates(for source: String, target: String, completion: @escaping CompletionHandler) {
        let items = [
            URLQueryItem(name: "source", value: source),
            URLQueryItem(name: "target", value: target)
        ]

        client.get(endpoint: ExchangeRatesEndpoint.rates, queryItems: items) { data, error in
            if let error = error {
                completion(nil, error)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let rates = try decoder.decode([Rate].self, from: data)
                    completion(RatesResponse(rates: rates), nil)
                } catch {
                    completion(nil, RestClientError.cannotDecode(error: error))
                }
            } else {
                completion(nil, RestClientError.noData)
            }
        }
    }

    // MARK: - Private

    // MARK: - Overridden
}
