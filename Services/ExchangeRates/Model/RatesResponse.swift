//
//  RatesResponse.swift
//  TransferWiseAPI
//
//  Created by Camilo Gaviria on 14/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

public struct RatesResponse {
    public let rates: [Rate]

    public var rate: Double? {
        return rates.first?.rate
    }
}

public class Rate: Decodable {
    let rate: Double
    let source: String
    let target: String
    let time: String
}
