//
//  Profile.swift
//  UserProfiles
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import Foundation

public enum UserProfileType: String, Decodable {
    case personal
    case business
}

public enum CompanyRole: String, Decodable {
    case owner = "OWNER"
    case director = "DIRECTOR"
    case other = "OTHER"
}

public enum CompanyType: String, Decodable {
    case limited = "LIMITED"
    case partnership = "PARTNERSHIP"
    case soleTrader = "SOLE_TRADER"
    case limitedByGuarantee = "LIMITED_BY_GUARANTEE"
    case limitedLiabilityCompany = "LIMITED_LIABILITY_COMPANY"
    case forProfitCorporation = "FOR_PROFIT_CORPORATION"
    case nonProfitCorporation = "NON_PROFIT_CORPORATION"
    case limitedPartnership = "LIMITED_PARTNERSHIP"
    case limitedLiabilityPartnership = "LIMITED_LIABILITY_PARTNERSHIP"
    case generalPartnership = "GENERAL_PARTNERSHIP"
    case soleProprietorship = "SOLE_PROPRIETORSHIP"
    case privateLimitedCompany = "PRIVATE_LIMITED_COMPANY"
    case publicLimitedCompany = "PUBLIC_LIMITED_COMPANY"
    case trust = "TRUST"
    case other = "OTHER"
}

public struct Profile: Decodable {
    let id: Int
    let type: UserProfileType
    let details: ProfileDetails
}

public struct ProfileDetails: Decodable {
    // Personal profile
    let firstName: String?
    let lastName: String?
    let dateOfBirth: Date?
    let phoneNumber: String?
    let avatar: String?
    let occupation: String?

    // Business profile
    let name: String?
    let registrationNumber: String?
    let acn: String?
    let abn: String?
    let arbn: String?
    let companyType: CompanyType?
    let companyRole: CompanyRole?
    let descriptionOfBusiness: String?
    let webpage: String?
    let businessCategory: String?
    let businessSubCategory: String?

    // Common
    let primaryAddress: Int?

    enum CodingKeys: CodingKey {
        // Personal
        case firstName
        case lastName
        case dateOfBirth
        case phoneNumber
        case avatar
        case occupation
        // Business
        case name
        case registrationNumber
        case acn
        case abn
        case arbn
        case companyType
        case companyRole
        case descriptionOfBusiness
        case webpage
        case businessCategory
        case businessSubCategory
        // Common
        case primaryAddress
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        // Personal profile
        firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try container.decodeIfPresent(String.self, forKey: .lastName)

        if let dateString = try container.decodeIfPresent(String.self, forKey: .dateOfBirth) {
            dateOfBirth = Date(dateString)
        } else {
            dateOfBirth = nil
        }

        phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
        avatar = try container.decodeIfPresent(String.self, forKey: .avatar)
        occupation = try container.decodeIfPresent(String.self, forKey: .occupation)

        // Business profile
        name = try container.decodeIfPresent(String.self, forKey: .name)
        registrationNumber = try container.decodeIfPresent(String.self, forKey: .registrationNumber)
        acn = try container.decodeIfPresent(String.self, forKey: .acn)
        abn = try container.decodeIfPresent(String.self, forKey: .abn)
        arbn = try container.decodeIfPresent(String.self, forKey: .arbn)
        companyType = try container.decodeIfPresent(CompanyType.self, forKey: .companyType)
        companyRole = try container.decodeIfPresent(CompanyRole.self, forKey: .companyRole)
        descriptionOfBusiness = try container.decodeIfPresent(String.self, forKey: .descriptionOfBusiness)
        webpage = try container.decodeIfPresent(String.self, forKey: .webpage)
        businessCategory = try container.decodeIfPresent(String.self, forKey: .businessCategory)
        businessSubCategory = try container.decodeIfPresent(String.self, forKey: .businessSubCategory)

        // Common
        primaryAddress = try container.decodeIfPresent(Int.self, forKey: .primaryAddress)
    }
}
