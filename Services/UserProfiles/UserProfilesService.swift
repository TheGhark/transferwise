//
//  UserProfilesService.swift
//  UserProfiles
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import Foundation

enum UserProfilesEndpoint: Pathable {
    case create

    var path: String {
        switch self {
        case .create:
            return "profiles"
        }
    }
}

public class UserProfileService {
    public typealias CreateProfileHandler = (Profile?, Error?) -> Void
    public typealias ProfilesHandler = ([Profile], Error?) -> Void

    private let client: RestClient
    private let dateFormatter = DateFormatter()

    public init(client: RestClient) {
        self.client = client

        dateFormatter.dateFormat = "YYYY-MM-DD"
    }

    public func profiles(completion: @escaping ProfilesHandler) {
        client.get(endpoint: UserProfilesEndpoint.create) { data, error in
            if let error = error {
                completion([], error)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let profiles = try decoder.decode([Profile].self, from: data)
                    completion(profiles, nil)
                } catch {
                    completion([], RestClientError.cannotDecode(error: error))
                }
            } else {
                completion([], RestClientError.noData)
            }
        }
    }

    public func create(type: ProfileType,
                       firstName: String,
                       lastName: String,
                       dateOfBirth: Date,
                       phoneNumber: String? = nil,
                       completion: @escaping CreateProfileHandler) {
        let data: [String: Any] = [
            "type": type.rawValue,
            "details": [
                "firstName": firstName,
                "lastName": lastName,
                "dateOfBirth": dateFormatter.string(from: dateOfBirth),
                "phoneNumber": phoneNumber
            ]
        ]

        client.post(endpoint: UserProfilesEndpoint.create, data: data, completion: { data, error in
            if let error = error {
                completion(nil, error)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let profile = try decoder.decode(Profile.self, from: data)
                    completion(profile, nil)
                } catch {
                    completion(nil, RestClientError.cannotDecode(error: error))
                }
            } else {
                completion(nil, RestClientError.noData)
            }

        })
    }
}
