//
//  CurrenciesResponse.swift
//  TransferWiseAPI
//
//  Created by Camilo Gaviria on 15/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Foundation

public struct CurrenciesResponse {
    public let currencies: [Currency]

    public init(currencies: [Currency]) {
        self.currencies = currencies
    }
}

public struct Currency: Decodable {
    public let code: String
    let hasBankDetails: Bool
    let payInAllowed: Bool
    public var name: String = ""

    enum CodingKeys: CodingKey {
        case code
        case hasBankDetails
        case payInAllowed
    }
}
