//
//  BorderlessAccountService.swift
//  ExchangeRatesTests
//
//  Created by Camilo Gaviria on 24/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import Foundation

public enum BorderlessAccountEndpoint: Pathable {
    case currencies

    public var path: String {
        switch self {
        case .currencies:
            return "borderless-accounts/balance-currencies"
        }
    }
}

open class BorderlessAccountService {
    // MARK: - Class Functions

    // MARK: - Properties

    public typealias CurrenciesCompletionHandler = (CurrenciesResponse?, Error?) -> Void
    private let client: RestClient

    // MARK: - Computed Properties

    // MARK: - Initialization

    public init(client: RestClient) {
        self.client = client
    }

    // MARK: - Public

    open func currencies(completion: @escaping CurrenciesCompletionHandler) {
        client.get(endpoint: BorderlessAccountEndpoint.currencies) { data, error in
            if let error = error {
                completion(nil, error)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let currencies = try decoder.decode([Currency].self, from: data)
                    let response = CurrenciesResponse(currencies: currencies)
                    completion(response, nil)
                } catch {
                    completion(nil, RestClientError.cannotDecode(error: error))
                }
            } else {
                completion(nil, RestClientError.noData)
            }
        }
    }

    // MARK: - Private

    // MARK: - Overridden
}
