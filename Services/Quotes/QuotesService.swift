//
//  QuotesService.swift
//  TransferWiseAPI
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import Foundation

public enum QuoteType: String, Decodable {
    case payout = "BALANCE_PAYOUT"
    case conversion = "BALANCE_CONVERSION"
    case regular = "REGULAR"
}

enum QuotesEndpoint: Pathable {
    case create

    var path: String {
        switch self {
        case .create:
            return "quotes"
        }
    }
}

public enum QuotesError: Error {
    case invalidAmounts
}

public class QuotesService {
    public typealias CreateQuoteHandler = (Quote?, Error?) -> Void

    // MARK: - Class Functions

    // MARK: - Properties

    private let client: RestClient

    // MARK: - Computed Properties

    // MARK: - Initialization

    public init(client: RestClient) {
        self.client = client
    }

    // MARK: - Public

    public func create(profile: Int,
                       source: String,
                       target: String,
                       rateType: String = "FIXED",
                       targetAmount: Double? = nil,
                       sourceAmount: Double? = nil,
                       type: QuoteType,
                       completion: @escaping CreateQuoteHandler) {
        var data: [String: Any] = [
            "profile": profile,
            "source": source,
            "target": target,
            "rateType": rateType,
            "type": type.rawValue
        ]

        if let targetAmount = targetAmount {
            data["targetAmount"] = targetAmount
        } else if let sourceAmount = sourceAmount {
            data["sourceAmount"] = sourceAmount
        } else {
            completion(nil, QuotesError.invalidAmounts)
        }

        client.post(endpoint: QuotesEndpoint.create, data: data, completion: { data, error in
            if let error = error {
                completion(nil, error)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let quote = try decoder.decode(Quote.self, from: data)
                    completion(quote, nil)
                } catch {
                    completion(nil, RestClientError.cannotDecode(error: error))
                }
            } else {
                completion(nil, RestClientError.noData)
            }
        })
    }

    // MARK: - Private

    // MARK: - Overridden
}
