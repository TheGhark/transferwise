//
//  Quote.swift
//  Quotes
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import Foundation

public enum QuoteError: Error {
    case invalidCreatedTime
    case deliveryEstimate
}

public struct Quote: Decodable {
    let id: Int
    let source: String
    let target: String
    let sourceAmount: Double
    let targetAmount: Double
    let type: QuoteType
    let rate: Double
    let createdTime: Date
    let createdByUserId: Int
    let profile: Int
    let rateType: String
    let deliveryEstimate: Date
    let fee: Double
    let allowedProfileTypes: [ProfileType]
    let guaranteedTargetAmount: Bool
    let ofSourceAmount: Bool

    enum CodingKeys: String, CodingKey {
        case id
        case source
        case target
        case sourceAmount
        case targetAmount
        case type
        case rate
        case createdTime
        case createdByUserId
        case profile
        case rateType
        case deliveryEstimate
        case fee
        case allowedProfileTypes
        case guaranteedTargetAmount
        case ofSourceAmount
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        source = try container.decode(String.self, forKey: .source)
        target = try container.decode(String.self, forKey: .target)
        sourceAmount = try container.decode(Double.self, forKey: .sourceAmount)
        targetAmount = try container.decode(Double.self, forKey: .targetAmount)
        type = try container.decode(QuoteType.self, forKey: .type)
        rate = try container.decode(Double.self, forKey: .rate)
        let createdTimeString = try container.decode(String.self, forKey: .createdTime)

        guard let createdTime = DateFormatter.iso8601Full.date(from: createdTimeString) else {
            throw QuoteError.invalidCreatedTime
        }
        self.createdTime = createdTime

        createdByUserId = try container.decode(Int.self, forKey: .createdByUserId)
        profile = try container.decode(Int.self, forKey: .profile)
        rateType = try container.decode(String.self, forKey: .rateType)

        let deliveryEstimateString = try container.decode(String.self, forKey: .deliveryEstimate)

        guard let deliveryEstimate = DateFormatter.iso8601Full.date(from: deliveryEstimateString) else {
            throw QuoteError.deliveryEstimate
        }
        self.deliveryEstimate = deliveryEstimate

        fee = try container.decode(Double.self, forKey: .fee)
        allowedProfileTypes = try container.decode([ProfileType].self, forKey: .allowedProfileTypes)
        guaranteedTargetAmount = try container.decode(Bool.self, forKey: .guaranteedTargetAmount)
        ofSourceAmount = try container.decode(Bool.self, forKey: .ofSourceAmount)
    }
}
