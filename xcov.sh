#!/bin/sh

xcodebuild -project TransferWiseAPI.xcodeproj -scheme TransferWise -sdk iphonesimulator -destination 'platform=iOS Simulator,name=iPhone SE,OS=13.3' test && xcov -p TransferWiseAPI.xcodeproj -s TransferWise -o xcov_output