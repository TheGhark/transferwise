//
//  QuotesServiceTests.swift
//  QuotesServiceTests
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import OHHTTPStubs
import OHHTTPStubsSwift
@testable import Quotes
import XCTest

class QuotesServiceTests: XCTestCase {
    var sut: QuotesService!

    override func setUp() {
        super.setUp()
        let bundle = Bundle(for: RestClient.self)
        let configuration = Configuration(bundle: bundle, name: "TWConfiguration")
        let client = RestClient(configuration: configuration)
        sut = QuotesService(client: client)
    }

    override func tearDown() {
        sut = nil
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testCreate() {
        guard let fileAtPath = OHPathForFile("quote-create.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        stub(condition: pathEndsWith(QuotesEndpoint.create.path)) { _ in
            HTTPStubsResponse(fileAtPath: fileAtPath, statusCode: 200, headers: nil)
        }

        let expectation = XCTestExpectation(description: "Create quote - Success")
        sut.create(profile: 5699,
                   source: "EUR",
                   target: "EUR",
                   targetAmount: 600,
                   type: .conversion) { quote, _ in
            expectation.fulfill()
            XCTAssertNotNil(quote)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCreate_ServerError() {
        stub(condition: pathEndsWith(QuotesEndpoint.create.path)) { _ in
            let notConnectedError = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue)
            return HTTPStubsResponse(error: notConnectedError)
        }

        let expectation = XCTestExpectation(description: "Create quote - server error")
        sut.create(profile: 5699,
                   source: "EUR",
                   target: "EUR",
                   targetAmount: 600,
                   type: .conversion) { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.service(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCreate_CannotDecode() {
        guard let fileAtPath = OHPathForFile("quote-create-invalid.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        stub(condition: pathEndsWith(QuotesEndpoint.create.path)) { _ in
            HTTPStubsResponse(fileAtPath: fileAtPath, statusCode: 200, headers: nil)
        }

        let expectation = XCTestExpectation(description: "Create quote - Cannot decode")
        sut.create(profile: 5699,
                   source: "EUR",
                   target: "EUR",
                   targetAmount: 600,
                   type: .conversion) { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.cannotDecode(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }
}
