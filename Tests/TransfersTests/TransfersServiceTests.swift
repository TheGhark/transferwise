//
//  TransfersServiceTests.swift
//  TransfersTests
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import OHHTTPStubs
import OHHTTPStubsSwift
@testable import Transfers
import XCTest

class TransfersServiceTests: XCTestCase {
    var sut: TransfersService!

    override func setUp() {
        super.setUp()
        let bundle = Bundle(for: RestClient.self)
        let configuration = Configuration(bundle: bundle, name: "TWConfiguration")
        let client = RestClient(configuration: configuration)
        sut = TransfersService(client: client)
    }

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        sut = nil
        super.tearDown()
    }

    func testCreate() {
        guard let filePath = OHPathForFile("transfers-create.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        let expectation = XCTestExpectation(description: "Transfer create - success")

        stub(condition: pathEndsWith(TransfersEndpoint.create.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath,
                              statusCode: 200,
                              headers: nil)
        }

        sut.create(targetAccount: 13_796_745,
                   quote: 1_103_731,
                   customerTransactionId: "1766f9bc-3e83-11ea-b77f-2e728ce88125",
                   reference: "Transfer test - success",
                   transferPurpose: "Might be needed",
                   sourceOfFunds: "Might be needed") { transfer, _ in
            expectation.fulfill()
            XCTAssertNotNil(transfer)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCreate_ServerError() {
        let expectation = XCTestExpectation(description: "Transfer create - failure server")

        stub(condition: pathEndsWith(TransfersEndpoint.create.path)) { _ in
            let notConnectedError = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue)
            return HTTPStubsResponse(error: notConnectedError)
        }

        sut.create(targetAccount: 13_796_745,
                   quote: 1_103_731,
                   customerTransactionId: "1766f9bc-3e83-11ea-b77f-2e728ce88125",
                   reference: "Transfer test - success",
                   transferPurpose: "Might be needed",
                   sourceOfFunds: "Might be needed") { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.service(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCreate_CannotDecode() {
        guard let filePath = OHPathForFile("transfers-create-invalid.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        let expectation = XCTestExpectation(description: "Transfer create - failure cannot decode")

        stub(condition: pathEndsWith(TransfersEndpoint.create.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath,
                              statusCode: 200,
                              headers: nil)
        }

        sut.create(targetAccount: 13_796_745,
                   quote: 1_103_731,
                   customerTransactionId: "1766f9bc-3e83-11ea-b77f-2e728ce88125",
                   reference: "Transfer test - success",
                   transferPurpose: "Might be needed",
                   sourceOfFunds: "Might be needed") { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.cannotDecode(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testFund() {
        guard let filePath = OHPathForFile("transfers-fund.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        let expectation = XCTestExpectation(description: "Fund transfer - success")
        let profileId = 5699
        let transferId = 47_865_002
        stub(condition: pathEndsWith(FundEndpoint.fund(profileId: profileId, transferId: transferId).path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath, statusCode: 200, headers: nil)
        }

        sut.fund(transfer: transferId, profile: profileId) { response, _ in
            expectation.fulfill()
            XCTAssertNotNil(response)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testFund_ServerError() {
        let expectation = XCTestExpectation(description: "Fund transfer - server error")
        let profileId = 5699
        let transferId = 47_865_002

        stub(condition: pathEndsWith(FundEndpoint.fund(profileId: profileId, transferId: transferId).path)) { _ in
            let notConnectedError = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue)
            return HTTPStubsResponse(error: notConnectedError)
        }

        sut.fund(transfer: transferId, profile: profileId) { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.service(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testFund_CannotDecode() {
        guard let filePath = OHPathForFile("transfers-fund-invalid.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        let expectation = XCTestExpectation(description: "Fund transfer - failure cannot decode")
        let profileId = 5699
        let transferId = 47_865_002

        stub(condition: pathEndsWith(FundEndpoint.fund(profileId: profileId, transferId: transferId).path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath, statusCode: 200, headers: nil)
        }

        sut.fund(transfer: transferId, profile: profileId) { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.cannotDecode(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }
}
