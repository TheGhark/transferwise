//
//  OpenExchangeRatesTests.swift
//  OpenExchangeRatesTests
//
//  Created by Camilo Gaviria on 24/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import OHHTTPStubs
import OHHTTPStubsSwift
@testable import OpenExchangeRates
import XCTest

class OpenExchangeRatesTests: XCTestCase {
    var sut: OpenExchangeRatesService!

    override func setUp() {
        super.setUp()
        let bundle = Bundle(for: OpenExchangeRatesService.self)
        let configuration = Configuration(bundle: bundle, name: "OERConfiguration")
        let client = RestClient(configuration: configuration)
        sut = OpenExchangeRatesService(client: client)
    }

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        sut = nil
        super.tearDown()
    }

    func testCurrencies() {
        guard let filePath = OHPathForFile("currencies-names.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        stub(condition: pathEndsWith(OpenExchangeRatesEndpoint.currencies.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath, statusCode: 200, headers: nil)
        }

        let expectation = XCTestExpectation(description: "Open Exchange Rates Currencies - success")
        sut.currencies { data, _ in
            expectation.fulfill()
            XCTAssert(data?.isEmpty == false)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCurrencies_ServerError() {
        let expectation = XCTestExpectation(description: "Open Exchange Rates Currencies - server error")

        stub(condition: pathEndsWith(OpenExchangeRatesEndpoint.currencies.path)) { _ in
            let notConnectedError = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue)
            return HTTPStubsResponse(error: notConnectedError)
        }

        sut.currencies { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.service(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }
}
