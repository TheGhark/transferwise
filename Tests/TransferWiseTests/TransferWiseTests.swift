//
//  TransferWiseTests.swift
//  TransferWiseTests
//
//  Created by Camilo Gaviria on 24/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

@testable import TransferWise
import XCTest

class TransferWiseTests: XCTestSuite {
    let suites: [XCTestSuite] = [
        XCTestSuite(forTestCaseClass: OpenExchangeRatesTests.self),
        XCTestSuite(forTestCaseClass: BorderlessAccountTests.self),
        XCTestSuite(forTestCaseClass: ExchangeRatesTests.self),
        XCTestSuite(forTestCaseClass: TransfersServiceTests.self),
        XCTestSuite(forTestCaseClass: QuotesServiceTests.self),
        XCTestSuite(forTestCaseClass: UserProfilesTests.self)
    ]

    override func setUp() {
        super.setUp()

        for suite in suites {
            suite.run()
        }
    }
}
