//
//  UserProfilesTests.swift
//  UserProfilesTests
//
//  Created by Camilo Gaviria on 23/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
import OHHTTPStubs
import OHHTTPStubsSwift
@testable import UserProfiles
import XCTest

class UserProfilesTests: XCTestCase {
    private var sut: UserProfileService!

    override func setUp() {
        super.setUp()
        let bundle = Bundle(for: RestClient.self)
        let configuration = Configuration(bundle: bundle, name: "TWConfiguration")
        let client = RestClient(configuration: configuration)

        sut = UserProfileService(client: client)
    }

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        sut = nil
        super.tearDown()
    }

    func testCreate() {
        guard let dateOfBirth = Date("1988-03-31") else {
            preconditionFailure("Date of birth is required")
        }

        guard let filePath = OHPathForFile("user-profile-create.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        stub(condition: pathEndsWith(UserProfilesEndpoint.create.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath, statusCode: 200, headers: nil)
        }

        let expectation = XCTestExpectation(description: "Create personal profile - success")

        sut.create(type: .personal,
                   firstName: "Camilo",
                   lastName: "Rodriguez Gaviria",
                   dateOfBirth: dateOfBirth) { data, _ in
            expectation.fulfill()
            XCTAssertNotNil(data)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCreate_ServerError() {
        guard let dateOfBirth = Date("1988-03-31") else {
            preconditionFailure("Date of birth is required")
        }

        stub(condition: pathEndsWith(UserProfilesEndpoint.create.path)) { _ in
            let notConnectedError = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue)
            return HTTPStubsResponse(error: notConnectedError)
        }

        let expectation = XCTestExpectation(description: "Create personal profile - server error")

        sut.create(type: .personal,
                   firstName: "Camilo",
                   lastName: "Rodriguez Gaviria",
                   dateOfBirth: dateOfBirth) { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.service(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCreate_CannotDecode() {
        guard let dateOfBirth = Date("1988-03-31") else {
            preconditionFailure("Date of birth is required")
        }

        guard let filePath = OHPathForFile("user-profile-create-invalid.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        stub(condition: pathEndsWith(UserProfilesEndpoint.create.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath, statusCode: 200, headers: nil)
        }

        let expectation = XCTestExpectation(description: "Create personal profile - cannot decode")

        sut.create(type: .personal,
                   firstName: "Camilo",
                   lastName: "Rodriguez Gaviria",
                   dateOfBirth: dateOfBirth) { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.cannotDecode(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testProfiles() {
        guard let filePath = OHPathForFile("user-profile-list.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        stub(condition: pathEndsWith(UserProfilesEndpoint.create.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath, statusCode: 200, headers: nil)
        }

        let expectation = XCTestExpectation(description: "User profiles list - success")

        sut.profiles { data, _ in
            expectation.fulfill()
            XCTAssertNotNil(data)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testProfiles_ServerError() {
        stub(condition: pathEndsWith(UserProfilesEndpoint.create.path)) { _ in
            let error = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue, userInfo: nil)
            return HTTPStubsResponse(error: error)
        }

        let expectation = XCTestExpectation(description: "User profiles list - server error")

        sut.profiles { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.service(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testProfiles_CannotDecode() {
        guard let filePath = OHPathForFile("user-profile-list-invalid.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        stub(condition: pathEndsWith(UserProfilesEndpoint.create.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath, statusCode: 200, headers: nil)
        }

        let expectation = XCTestExpectation(description: "User profiles list - cannot decode")

        sut.profiles { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.cannotDecode(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }
}
