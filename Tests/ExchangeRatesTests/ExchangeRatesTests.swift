//
//  ExchangeRatesTests.swift
//  ExchangeRatesTests
//
//  Created by Camilo Gaviria on 24/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
@testable import ExchangeRates
import OHHTTPStubs
import OHHTTPStubsSwift
import XCTest

class ExchangeRatesTests: XCTestCase {
    var sut: ExchangeRatesService!

    override func setUp() {
        super.setUp()
        let bundle = Bundle(for: RestClient.self)
        let configuration = Configuration(bundle: bundle, name: "TWConfiguration")
        let client = RestClient(configuration: configuration)
        sut = ExchangeRatesService(client: client)
    }

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        sut = nil
        super.tearDown()
    }

    func testRates() {
        guard let filePath = OHPathForFile("exchange-rates-list.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        let expectation = XCTestExpectation(description: "Exchange Rates List - success")

        stub(condition: pathEndsWith(ExchangeRatesEndpoint.rates.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath,
                              statusCode: 200,
                              headers: nil)
        }

        sut.rates(for: "COP", target: "PLN") { response, _ in
            expectation.fulfill()
            XCTAssert(response?.rates.isEmpty == false)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testRates_ServerError() {
        let expectation = XCTestExpectation(description: "Exchange Rates List - server error")

        stub(condition: pathEndsWith(ExchangeRatesEndpoint.rates.path)) { _ in
            let notConnectedError = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue)
            return HTTPStubsResponse(error: notConnectedError)
        }

        sut.rates(for: "COP", target: "PLN") { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.service(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testRates_CannotDecode() {
        guard let filePath = OHPathForFile("exchange-rates-list-invalid.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        let expectation = XCTestExpectation(description: "Exchange Rates List - cannot decode")

        stub(condition: pathEndsWith(ExchangeRatesEndpoint.rates.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath, statusCode: 200, headers: nil)
        }

        sut.rates(for: "COP", target: "PLN") { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.cannotDecode(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }
}
