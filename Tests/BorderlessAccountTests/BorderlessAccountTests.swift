//
//  BorderlessAccountTests.swift
//  BorderlessAccountTests
//
//  Created by Camilo Gaviria on 24/01/2020.
//  Copyright © 2020 heima. All rights reserved.
//

import Base
@testable import BorderlessAccount
import OHHTTPStubs
import OHHTTPStubsSwift
import XCTest

class BorderlessAccountTests: XCTestCase {
    var sut: BorderlessAccountService!

    override func setUp() {
        super.setUp()
        let bundle = Bundle(for: RestClient.self)
        let configuration = Configuration(bundle: bundle, name: "TWConfiguration")
        let client = RestClient(configuration: configuration)
        sut = BorderlessAccountService(client: client)
    }

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        sut = nil
        super.tearDown()
    }

    func testCurrencies() {
        guard let filePath = OHPathForFile("currencies.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        let expectation = XCTestExpectation(description: "Currencies - success")

        stub(condition: pathEndsWith(BorderlessAccountEndpoint.currencies.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath,
                              statusCode: 200,
                              headers: nil)
        }

        sut.currencies { response, _ in
            expectation.fulfill()
            XCTAssert(response?.currencies.isEmpty == false)
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCurrencies_ServerError() {
        let expectation = XCTestExpectation(description: "Currencies - failure server")

        stub(condition: pathEndsWith(BorderlessAccountEndpoint.currencies.path)) { _ in
            let notConnectedError = NSError(domain: NSURLErrorDomain, code: URLError.notConnectedToInternet.rawValue)
            return HTTPStubsResponse(error: notConnectedError)
        }

        sut.currencies { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.service(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }

    func testCurrencies_CannotDecode() {
        guard let filePath = OHPathForFile("currencies-invalid.json", type(of: self)) else {
            preconditionFailure("File not found")
        }

        let expectation = XCTestExpectation(description: "Currencies - failure cannot decode")

        stub(condition: pathEndsWith(BorderlessAccountEndpoint.currencies.path)) { _ in
            HTTPStubsResponse(fileAtPath: filePath,
                              statusCode: 200,
                              headers: nil)
        }

        sut.currencies { _, error in
            expectation.fulfill()
            XCTAssertTrue((error as? RestClientError) == RestClientError.cannotDecode(error: nil))
        }

        wait(for: [expectation], timeout: 1)
    }
}
